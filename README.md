# First Screen Task Solution

repo containing solution for the 1 screen Task

## Loading data

data is loaded to a mongo database instance by a one line bash command:

```sh
wget -O - -o /dev/null http://data.githubarchive.org/2015-01-01-15.json.gz | zcat | mongoimport -c events -d <db_name>
```

you can also run `npm run load-fixtures`.

## Running

set your config in `.env` file or the environment variables

example:
```sh
export NODE_ENV=production
export PORT=4040
export MONGO_HOST=mongodb://localhost/1screen
export MONGO_PORT=27017
```

the make sure mongodb is running wherever the it is set in the config andd node & npm installed then run

```sh
cd /path/to/project
npm install
npm start
```

## Running with Docker

just make sure docker is installed & running and docker-compose is also installed (and port 4040 is free) and run

```sh
cd /path/to/project
docker-compose build
docker-compose up
```

Using docker will automatically load the database with the github data, install the requirements and run the server.

## Generating Documentation

Documentation is written with ApiDoc to generate HTML Docs make sure you have the npm deps installed and run:

```sh
npm run gen-docs
```

docs will be available in the docs directory
