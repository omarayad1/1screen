import Joi from 'joi';

export default {
  // GET /api/events
  filter: {
    query: {
      repo_id: Joi.number().required(),
      type: Joi.string().required()
    }
  },

  // GET /api/events/actor/:login
  actorDetails: {
    params: {
      login: Joi.string().required()
    }
  },

  // DELETE /api/events/actor/:login
  delHistory: {
    params: {
      login: Joi.string().required()
    }
  },

  // GET /api/events/actor/:login/actions
  eventsNo: {
    params: {
      login: Joi.string().required()
    }
  }
};
