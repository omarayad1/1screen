import Event from '../models/event.model';

function filter(req, res) {
  const { repo_id, type } = req.query;
  Event
    .find({ 'repo.id': repo_id, type })
    .then(results => res.json(results))
    .catch(err => res.json(err));
}

function actorDetails(req, res) {
  const { login } = req.params;
  Event
    .aggregate([
      {
        $match: { 'actor.login': login }
      },
      {
        $group: { _id: '$actor.id', repos: { $addToSet: '$repo' }, actor: { $first: '$actor' } }
      },
      {
        $project: { repos: true, actor: true, _id: false }
      }
    ])
    .then((repos) => {
      if (repos.length) {
        return res.json(repos[0]);
      }
      return res.status(404).send('User not found');
    })
    .catch(err => res.json(err));
}

function eventsNo(req, res) {
  const { login } = req.params;
  Event
    .aggregate([
      {
        $match: { 'actor.login': login }
      },
      {
        $sort: { created_at: -1 }
      },
      {
        $group: {
          _id: { actor: '$actor.id', repo: '$repo.id' },
          count: { $sum: 1 },
          repo: { $first: '$repo' },
          actor: { $first: '$actor' },
          last_event: { $first: '$created_at' }
        }
      },
      {
        $sort: { count: -1, last_event: -1 }
      },
      {
        $project: { repo: true, actor: true, count: true, last_event: true, _id: false }
      },
      {
        $limit: 1
      }
    ])
    .then((repo) => {
      if (repo.length) {
        return res.json(repo[0]);
      }
      return res.status(404).send('User not found');
    })
    .catch(err => res.json(err));
}

function topActor(req, res) {
  Event
    .aggregate([
      {
        $group: {
          _id: { actor: '$actor.id', repo: '$repo.id' },
          count: { $sum: 1 },
          repo: { $first: '$repo' },
          actor: { $first: '$actor' },
        }
      },
      {
        $sort: { count: -1 }
      },
      {
        $group: {
          _id: '$_id.repo',
          count: { $max: '$count' },
          repo: { $first: '$repo' },
          actor: { $first: '$actor' }
        }
      },
      {
        $project: { repo: true, actor: true, count: true, _id: false }
      }
    ])
    .then(repos => res.json(repos))
    .catch(err => res.json(err));
}

function delHistory(req, res) {
  const { login } = req.params;
  Event
    .remove({ 'actor.login': login })
    .then((docs) => {
      if (docs.result.n) {
        return res.json(docs);
      }
      return res.status(404).send('User not found');
    })
    .catch(err => res.json(err));
}

export default { filter, actorDetails, eventsNo, topActor, delHistory };
