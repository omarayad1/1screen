import { Router as expressRouter } from 'express';
import validate from 'express-validation';
import eventCtrl from '../controllers/event.controller';
import paramValidation from '../validators/event.validator';

const router = expressRouter();

/**
 * @apiDefine UserNotFoundError
 *
 * @apiError UserNotFound The login of the User was not found.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     User not found
 */

router.route('/')
  /**
   * @api {get} /api/events Filter events by repo id & event type
   * @apiGroup Events
   * @apiParam {Number} repo_id repo id
   * @apiParam {String} type event type
   * @apiParamExample {json} Input
   *    {
   *      "repo_id": 28688495,
   *      "type": "CreateEvent"
   *    }
   * @apiSuccess {Object[]} events Event's list
   * @apiSuccess {Number} events.id Event id
   * @apiSuccess {String} events._id Event DB id
   * @apiSuccess {String} events.type Event title
   * @apiSuccess {Object} events.payload Event Payload
   * @apiSuccess {Boolean} events.public Event public?
   * @apiSuccess {Date} events.created_at Event's date
   * @apiSuccess {Object} events.repo Event's repo details
   * @apiSuccess {Object} events.actor Event's actor details
   * @apiSuccessExample {json} Success
   *    HTTP/1.1 200 OK
   *    [{
   *        _id: "593c1e6efc8cf841b7edccf4",
   *        id: 2489651045,
   *        type: "CreateEvent",
   *        payload: {
   *          pusher_type: "user",
   *          description: "Solution to homework and assignments from MIT",
   *          master_branch: "master",
   *          ref_type: "branch",
   *          ref: "master"
   *        },
   *        public: true,
   *        created_at: "2015-01-01T15:00:00.000Z",
   *        repo: {
   *          id: 28688495,
   *          name: "petroav/6.828",
   *          url: "https://api.github.com/repos/petroav/6.828"
   *        },
   *        actor: {
   *          id: 665991,
   *          login: "petroav",
   *          gravatar_id: "",
   *          url: "https://api.github.com/users/petroav",
   *          avatar_url: "https://avatars.githubusercontent.com/u/665991?"
   *        }
   *      }
   *    ]
   */
  .get(validate(paramValidation.filter), eventCtrl.filter);

router.route('/actor/:login')
  /**
   * @api {get} /api/events/actor/:login get user's contributions by user's login
   * @apiGroup Events
   * @apiSuccess {Object} contributions
   * @apiSuccess {Object[]} contributions.repos list of repos the user contributes to
   * @apiSuccess {Object} contributions.actor actor details
   * @apiSuccessExample {json} Success
   *    HTTP/1.1 200 OK
   *    {
   *      repos: [
   *        {
   *          id: 16438233,
   *          name: "mapillary/mapillary_issues",
   *          url: "https://api.github.com/repos/mapillary/mapillary_issues"
   *        }, {
   *          id: 1725199,
   *          name: "github/linguist",
   *          url: "https://api.github.com/repos/github/linguist"
   *        }, {
   *          id: 28688327,
   *          name: "floscher/linguist",
   *          url: "https://api.github.com/repos/floscher/linguist"
   *        }
   *      ],
   *      actor: {
   *        id: 3904348,
   *        login: "floscher",
   *        gravatar_id: "",
   *        url: "https://api.github.com/users/floscher",
   *        avatar_url: "https://avatars.githubusercontent.com/u/3904348?"
   *      }
   *    }
   *
   * @apiUse UserNotFoundError
   */
  .get(validate(paramValidation.actorDetails), eventCtrl.actorDetails)
  /**
   * @api {delete} /api/events/actor/:login removes all user histroy by login
   * @apiGroup Events
   * @apiSuccess {Object} result
   * @apiSuccess {Number} result.n number of records for this user
   * @apiSuccess {Boolean} result.ok delete success ?
   * @apiSuccessExample {json} Success
   *    HTTP/1.1 200 OK
   *    {
   *      "n": 26,
   *      "ok": 1
   *    }
   *
   * @apiUse UserNotFoundError
   */
  .delete(validate(paramValidation.delHistory), eventCtrl.delHistory);

router.route('/actor/:login/actions')
  /**
   * @api {get} /api/events/actor/:login/actions
   * gets repo with highest number of events from an actor
   * @apiGroup Events
   * @apiSuccess {Object} mostContributed
   * @apiSuccess {Object} mostContributed.repo details of the repo the user contributes the most
   * @apiSuccess {Object} mostContributed.actor actor details
   * @apiSuccess {Date} mostContributed.last_event time of the last event for this repo
   * @apiSuccess {Number} mostContributed.count Number of events this user contributed to this repo
   * @apiSuccessExample {json} Success
   *    HTTP/1.1 200 OK
   *    {
   *      count: 24,
   *      repo: {
   *        id: 28688327,
   *        name: "floscher/linguist",
   *        url: "https://api.github.com/repos/floscher/linguist"
   *      },
   *      actor: {
   *        id: 3904348,
   *        login: "floscher",
   *        gravatar_id: "",
   *        url: "https://api.github.com/users/floscher",
   *        avatar_url: "https://avatars.githubusercontent.com/u/3904348?"
   *      },
   *      last_event: "2015-01-01T15:46:40Z"
   *    }
   *
   * @apiUse UserNotFoundError
   */
  .get(validate(paramValidation.eventsNo), eventCtrl.eventsNo);

router.route('/actor')
  /**
   * @api {get} /api/events/actor gets all repos with the user who contributed the most
   * @apiGroup Events
   * @apiSuccess {Object[]} repos
   * @apiSuccess {Object} repos.repo details of the repo
   * @apiSuccess {Object} repos.actor actor details
   * @apiSuccess {Number} repos.count Number of events this user contributed to this repo
   * @apiSuccessExample {json} Success
   *    HTTP/1.1 200 OK
   *    [
   *      {
   *        count: 1,
   *        repo: {
   *          id: 24043280,
   *          name: "sshuttle/sshuttle",
   *          url: "https://api.github.com/repos/sshuttle/sshuttle"
   *        },
   *        actor: {
   *          id: 323695,
   *          login: "storrgie",
   *          gravatar_id: "",
   *          url: "https://api.github.com/users/storrgie",
   *          avatar_url: "https://avatars.githubusercontent.com/u/323695?"
   *        }
   *      }, {
   *        count: 1,
   *        repo: {
   *          id: 28689183,
   *          name: "arisu/VCppMFC_Samples",
   *          url: "https://api.github.com/repos/arisu/VCppMFC_Samples"
   *        },
   *        actor: {
   *          id: 448126,
   *          login: "arisu",
   *          gravatar_id: "",
   *          url: "https://api.github.com/users/arisu",
   *          avatar_url: "https://avatars.githubusercontent.com/u/448126?"
   *        }
   *      }
   *    ]
   */
  .get(eventCtrl.topActor);

export default router;
