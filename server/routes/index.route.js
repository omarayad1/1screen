import { Router as expressRouter } from 'express';
import eventRoutes from './event.route';

const router = expressRouter();

/** GET /health-check - Check service health */
router.get('/health-check', (req, res) =>
  res.send('OK')
);

router.use('/events', eventRoutes);

export default router;
