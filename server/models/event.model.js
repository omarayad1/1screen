import mongoose, { Schema } from 'mongoose';

const EventSchema = new Schema({
  id: {
    type: Number,
    unique: true,
    required: true
  },
  type: {
    type: String,
    required: true
  },
  actor: {
    id: Number,
    login: String,
    gravatar_id: String,
    url: String,
    avatar_url: String
  },
  repo: {
    id: Number,
    name: String,
    url: String
  },
  payload: Schema.Types.Mixed,
  public: Boolean,
  created_at: Date
});

export default mongoose.model('Event', EventSchema);
